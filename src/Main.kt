import java.io.File
import java.io.FileNotFoundException

fun main(args: Array<String>) {
    Main().spellChecker()
}

class Main {
    val dictionary = Dictionary("C:/Users/junio/IdeaProjects/projeto_pp/res/dictionary.txt")
    val user = User("C:/Users/junio/IdeaProjects/projeto_pp/res/input.txt",
            "C:/Users/junio/IdeaProjects/projeto_pp/res/user_output.txt")

    init {
        dictionary.load()
    }

    fun spellChecker() {
        user.words.forEach { word ->
            check(word)
        }
        user.save()
        dictionary.save()
    }

    fun check(word: String) {
        if (!dictionary.contains(word)) {
            print("A palavra \"$word\" não contém no dicionário. Entre com a opção desejada:\n" +
                    "1 - Aceitar (A palavra será adicionada ao dicionário)\n" +
                    "2 - Ignorar (Todas as ocorrências dessa palavra será ignorada)\n" +
                    "3 - Substituir (Escolher uma nova palavra para substitui-la\n")

            valid(readLine(), word)

        } else {
            user.wordsOut.add(word)
        }
    }

    fun valid(s: String?, word: String) {

        when (s?.trim()) {
            "1" -> {
                dictionary.addWord(word)
                println("Palavra adicionada com sucesso")
            }

            "2" -> {
                dictionary.ignoredWords.add(word)
                user.wordsOut.add(word)
                print("Palavra adicionada a lista de ignoradas")
            }

            "3" -> {
                print("Digite a nova palavra: ")
                val w = readLine() ?: ""

                if (dictionary.contains(w)) {
                    user.wordsOut.add(w)
                    println("Palavra alterada com sucesso")

                    user.words.forEachIndexed { index, s0 ->
                        if (s0 == word) {
                            user.words[index] = w
                        }
                    }

                } else {
                    check(w)
                }
            }

            else -> {
                println("Opção seleciona não correnponde a nenhuma opção. Informe um valor entre 1 e 3:")
                val a = readLine() ?: ""
                valid(word, a)
            }
        }
    }

}

data class Dictionary(private val path: String) : WordSet() {

    /**
     * @throws Exception se o caminho não for válido
     */
    init {
        if (path.isBlank()) {//validando nome do arquivo passado no construtor
            throw FileNotFoundException("Caminho do arquivo não passado")
        }
    }

    /**
     * Adiciona a [words] a lista de palavras contidas no arquivo em [path]
     *
     */
    fun load() {
        try {
            val inputStream = File(path).inputStream()//obtendo o input stream do arquivo
            words.clear()//limpando a lista de palavras

            //lendo linha por linha e adicionando na lista de palavras
            inputStream.bufferedReader().useLines { it.forEach { words.add(it) } }
        } catch (e: FileNotFoundException) {
            println(e.message)
        }
    }


    /**
     * Salva [words], substituindo o arquivo original
     */
    fun save() {
        //words.sort()
        File(path).printWriter().use { out ->
            words.forEach { out.println(it) }
        }
    }

}

open class WordSet {

    protected val words: MutableList<String> = mutableListOf()//lista de palavras do dicionário
    val ignoredWords = mutableListOf<String>()

    /**
     * @param word palavra a ser buscada
     * @return se [word] pertence ao dicionário
     */
    fun contains(word: String) = words.contains(word) || ignoredWords.contains(word)


    /**
     * @param word palavra a ser adicionada ao docionário
     * adiciona [word] ao dicionário
     */
    fun addWord(word: String): Boolean {
        return if (!words.contains(word)) {
            words.add(word)
            true
        } else {
            false
        }
    }
}

data class User(private val path: String, private val pathOut: String) {

    lateinit var words: MutableList<String>
    var wordsOut = mutableListOf<String>()

    init {
        if (path.isBlank()) {
            throw Exception("Caminho do arquivo não passado")
        } else {
            load()
        }
    }

    /**
     * Adiciona a [words] a lista de palavras contidas no arquivo em [path]
     */
    private fun load() {
        words = mutableListOf()

        try {
            val inputStream = File(path).inputStream()//obtendo o input stream do arquivo

            //lendo string por string e adicionando na lista de palavras
            inputStream.bufferedReader().useLines {
                it.forEach {
                    val a = it.toLowerCase().split(" ")
                    words.addAll(a)
                }
            }
        } catch (e: FileNotFoundException) {
            println(e.message)
        }
    }

    fun save() {

        val f = File(pathOut)
        f.createNewFile()
        f.printWriter().use { out ->
            wordsOut.forEach { out.print("$it ") }
        }
    }

}